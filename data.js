var fs = require("fs");

var result = [];
var schoolCount = [];
var moiDistrict = [];
var lang = [];
var cat1 =  [];

const csvFilePath = "primaryschool.csv"
const csv = require("csvtojson")
csv()
.fromFile(csvFilePath)
.on("json", (jsonObj) => {

    result.push(jsonObj);

    if(lang.indexOf(jsonObj["moi"]) == -1 && jsonObj["moi"] != "") {
        lang.push(jsonObj.moi);
    }
    if(cat1.indexOf(jsonObj["cat"]) == -1 && jsonObj["cat"] != "") {
        cat1.push(jsonObj.cat);
    }

})
.on("done",(error) => {

    schoolCount = convertToArray(schlCount(result));
    var catDistrict = inputcategory(catCount(result));
    moiDistrict = inputmoi(moi(result));
    var finalobj = {sol1 : schoolCount, sol2 : catDistrict, sol3 : moiDistrict};

    fs.writeFile("public/index.json", JSON.stringify(finalobj), (err) => {
        if (err) throw err;
        console.log('The file has been saved');
    })

})
/*function to fetch School Count*/
function schlCount(obj) {

    var schl= {};
    for(let obj1 of obj) {
        if(!schl[obj1["district_name"]])
            schl[obj1["district_name"]] = 0;
        schl[obj1["district_name"]] += 1;
    }
    return schl;

}
/*function to convert Object into Array*/
function convertToArray(obj) {

    var res = [];
    for (let key in obj) {
        res.push([key,obj[key]]);
    }
    return res;

}
/*function to Fetch Category Count*/
function catCount(obj) {

    var catd = {};
    for(let key of obj) {
        dist = key["district_name"];
        cat = key["cat"];
        if(!catd[dist])
            catd[dist] = {};
        if(!catd[dist][cat])
            catd[dist][cat] = 0;
        catd[dist][cat] += 1;
    }
    return catd;

}
function inputcategory(obj) {

    var district = Object.keys(obj);
    var series = cat1.map(function(elem) {
        return {name:elem, data:district.map(function(dist) {
            if(obj[dist][elem])
                return obj[dist][elem];
            return 0;
        })}
    })
    return {x:district, ser:series};

}
/*function to Fetch Moi Count*/
function moi(obj) {

    var moiD = {};
    for(let key of obj) {
        dist = key["district_name"];
        moi = key["moi"];
        if(!moiD[dist])
            moiD[dist] = {};
        if(!moiD[dist][moi])
            moiD[dist][moi] = 0;
        moiD[dist][moi] += 1;
    }
    return moiD;

}
function inputmoi(obj) {

    var districts = Object.keys(obj);
    var series = lang.map(function(elem) {
        return {name:elem, data:districts.map(function(dist) {
            if(obj[dist][elem])
                return obj[dist][elem];
            return 0;
        })}
    })
    return {x:districts, ser:series};

}


