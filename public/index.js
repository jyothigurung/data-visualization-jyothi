$(document).ready(function (){
$.ajax({type: "GET",url :"index.json",success:function(res) {

    Highcharts.chart('container3', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Total No. of Schools In Karnataka Grouped by MOI'
        },
        xAxis: {
            categories: res["sol3"]["x"]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No of Schools'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        series: res["sol3"]["ser"]
    });

    Highcharts.chart('container2', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Total No. of Schools In Karnataka Grouped by Category'
        },
        xAxis: {
            categories: res["sol2"]["x"]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No of Schools'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        series: res["sol2"]["ser"]
    });

    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
           text: 'Total No. Of Schools In Karnataka Per district'
       },
       subtitle: {
           text: ''
       },
       xAxis: {
           type: 'category',
           labels: {
               rotation: -45,
               style: {
                   fontSize: '13px',
                   fontFamily: 'Verdana, sans-serif'
               }
           }
       },
       yAxis: {
           min: 0,
           title: {
               text: 'No Of Schools'
           }
       },
       legend: {
           enabled: false
       },
       tooltip: {
           pointFormat: 'No Of Schools: <b>{point.y:.1f}</b>'
       },
       series: [{
           name: 'Population',
           data:  res["sol1"]                ,
           dataLabels: {
               enabled: true,
               rotation: -90,
               color: '#FFFFFF ',
               align: 'right',
               format: '{point.y:.1f}', // one decimal
               y: 10, // 10 pixels down from the top
               style: {
                   fontSize: '13px',
                   fontFamily: 'Verdana, sans-serif'
               }
            }
       }]
   });
}})
});
